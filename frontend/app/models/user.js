import Model, {attr, belongsTo, hasMany} from '@ember-data/model';
export default class UserModel extends Model {
  @attr('string') user_stub;
  @attr('string') name;
  @attr('string') email;
  @attr('string') password;
  @attr('string') phone;
  @attr('string') gender;
  @attr('string') account_type;
  @attr('number') balance;
  @attr('number') num_trans;
  @attr('number') num_beneficiaries;
  @attr('date')   created_date;
  @attr('date')    last_modified_date;
  @attr('boolean') is_admin

  // @hasMany('transactions') transactions;
  // @hasMany('beneficiaries') beneficiaries;
}
