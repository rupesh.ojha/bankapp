import Model, {attr, belongsTo} from '@ember-data/model';

export default class TransactionsModel extends Model {
  @attr('string') txn_stub;
  @attr('string') payer_stub;
  @attr('string') payee_stub;
  @attr('string') payer_email;
  @attr('string') payee_email;
  @attr('string') txn_type;
  @attr('number') txn_amt;
  @attr('string') txn_status;
  @attr('date') created_date;
  @attr('date') last_modified_date;

  // @belongsTo('user') user
}
