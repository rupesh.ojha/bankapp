import Model, {attr, belongsTo} from '@ember-data/model';

export default class BeneficiariesModel extends Model {
  // @belongsTo('user', { async: true, inverse: 'user' }) user;
  @attr('string') user_stub;
  @attr('string') beneficiary_stub;
  @attr('string') email;
  @attr('string') bank;
  @attr('string') ifsc;
  @attr('number') acct_id;

  // @belongsTo('user') user
}
