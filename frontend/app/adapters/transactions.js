// app/adapters/beneficiary.js
import JSONAPIAdapter from '@ember-data/adapter/json-api';
import { inject as service } from '@ember/service';

export default class TransactionsAdaptor extends JSONAPIAdapter {
  @service session;


  buildURL(modelName, id, snapshot, requestType, query) {
    let url = super.buildURL(...arguments);
    return `http://localhost:8080/BankingApp/users/${this.session.data.authenticated.user_stub}/transactions`;
  }

  createRecord(store, type, snapshot) {
    let data = snapshot._attributes
    let url = this.buildURL() + '/add'

    return this.ajax(url, 'POST', { data: data, headers: this.headers });
  }
  headers =  {
    'Authorization': `Bearer ${this.session.data.authenticated.token}`,
  }
}
