// app/adapters/application.js
import JSONAPIAdapter from '@ember-data/adapter/json-api';
import { inject as service } from '@ember/service';

export default class ApplicationAdapter extends JSONAPIAdapter {
  @service session;
  host = 'http://localhost:8080/BankingApp';
  headers =  {
    'Authorization': `Bearer ${this.session.data.authenticated.token}`,
  }
}


