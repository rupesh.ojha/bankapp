// app/adapters/beneficiary.js
import JSONAPIAdapter from '@ember-data/adapter/json-api';
import { inject as service } from '@ember/service';

export default class UserAdapter extends JSONAPIAdapter {
  @service session;

  buildURL(modelName, id, snapshot, requestType, query) {
    let url = super.buildURL(...arguments);
    return `http://localhost:8080/BankingApp/users/${this.session.data.authenticated.user_stub}`;
  }

  createRecord(store, type, snapshot) {
    let data = snapshot._attributes
    let url = this.buildURL()

    return this.ajax(url, 'POST', { data: data, headers: this.headers });
  }

  findAll(store, type, snapshot) {
    // let data = snapshot._attributes
    let url = `http://localhost:8080/BankingApp/admin/users`

    return this.ajax(url, 'GET', { headers: this.headers });
  }

  deleteRecord(store, type, snapshot, query) {
    let user_stub = snapshot.id
    let url = `http://localhost:8080/BankingApp/users/${user_stub}`

    return this.ajax(url, 'DELETE');
  }
  headers =  {
    'Authorization': `Bearer ${this.session.data.authenticated.token}`,
  }
}
