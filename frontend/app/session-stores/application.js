// app/session-stores/application.js
import RSVP from 'rsvp'
import Base from 'ember-simple-auth/session-stores/base';

export default class ApplicationSessionStore extends Base {
    init() {
        this._super(...arguments);
        this.clear();
    }
    persist(data) {
        this._data = JSON.stringify(data || {});
        return RSVP.resolve();
    }
    restore() {
        const data = JSON.parse(this._data) || {};

        return RSVP.resolve(data);
    }
    clear() {
        delete this._data;
        this._data = '{}';

        return RSVP.resolve();
    }
}
