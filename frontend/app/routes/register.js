import Route from '@ember/routing/route';
import {service} from "@ember/service";
import {tracked} from "@glimmer/tracking";
import {action} from "@ember/object";

export default class RegisterRoute extends Route {
  @service store
  @service router
  @tracked errorMessage

  @action
  async submitForm() {
    event.preventDefault();
    let formField = event.target.elements
    this.errorMessage = '';

    const data = {
      "user_stub": "",
      "name":formField.name.value,
      "email":formField.email.value,
      "password":formField.password.value,
      "phone":formField.phone.value,
      "gender":formField.gender.value,
      "account_type":formField.account_type.value,
      "balance": 0,
      "num_trans": 0,
      "num_beneficiaries": 0,
      "created_date": "",
      "last_modified_date": "",
      "is_admin": false
    };

    try {
      const res = await fetch('http://localhost:8080/BankingApp/Register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (res.ok) this.router.transitionTo("login");
    } catch (error) {
      console.error(error);
    }
  }

}
