import Route from '@ember/routing/route';
import {service} from "@ember/service";
import {action} from "@ember/object";

export default class AdminUsersRoute extends Route {
  @service session
  @service router;
  @service store;

  beforeModel(_transition) {
    if(!this.session.isAuthenticated) {
      this.router.replaceWith('/login')
    }
  }

  async model() {
    // Get the user_id parameter from the route's model
    let user_stub = this.session.data.authenticated.user_stub
    let users = await this.store.findAll('user')
    return {
      user_stub,
      users
    };
  }

  @action
  async deleteUser(user_stub) {
    event.preventDefault()

    let delR = this.store.peekRecord('user', user_stub)
    delR.deleteRecord()

    try {
      await delR.save();
      this.router.refresh()
      // this.router.transitionTo("users.user.transactions")
    } catch (error) {
      // this.errorMessage = error.errors[0].detail
      console.error(error)
      // newT.rollbackAttributes();
    }
  }
}
