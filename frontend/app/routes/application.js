// app/routes/application.js

import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class ApplicationRoute extends Route {
  @service session;
  @service router
  beforeModel() {
    window.onbeforeunload = function() {
      return "Data will be lost if you leave the page, are you sure?";
    }
    if (this.session.isAuthenticated) {
      const user_stub = this.session.data.authenticated.user_stub;
      this.router.replaceWith('users.user', user_stub);
    } else {
      // Redirect to the login route if not authenticated
      this.router.replaceWith('login');
    }
    return this.session.setup();
  }
}
