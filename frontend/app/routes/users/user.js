import Route from '@ember/routing/route';
import {service} from "@ember/service";

export default class UsersUserRoute extends Route {
  @service session
  @service router;
  @service store;

  beforeModel(transition) {
    if(!this.session.isAuthenticated) {
      this.router.replaceWith('/login')
    }
  }
  async model(params) {
    let user_stub = this.paramsFor('users.user').user_id;
    let [user, beneficiaries, transactions] = await Promise.all([
      this.store.findRecord('user', user_stub),
      this.store.findAll('beneficiaries', {user_id: user_stub}),
      this.store.findAll('transactions', {user_id: user_stub})
    ]);
    return {
      user,
      beneficiaries: Array.from(beneficiaries),
      transactions: Array.from(transactions)
    };
  }

  async afterModel(resolvedModel) {
    // Refresh the data
    console.log(resolvedModel)
    // await resolvedModel.user.reload();
  }
}
