import Route from '@ember/routing/route';
import {service} from "@ember/service";
import {action} from '@ember/object'
import {isEmpty} from "@ember/utils";

export default class UsersUserTransactionsAddRoute extends Route {
  @service store
  @service router

  @action
  async submitForm() {
    event.preventDefault()
    let formField = event.target.elements
    let user_stub = this.modelFor("users.user").user.user_stub;

    let newT = this.store.createRecord('transactions', {
      // "txn_stub": "",
      "payer_stub": user_stub,
      "payee_stub": form Field.txn_type.value === "deposit" || formField.txn_type.value === "withdraw" ? user_stub : formField.beneficiary.value.split(" ")[0],
      "payer_email": this.modelFor("users.user.transactions").user.email,
      "payee_email": formField.txn_type.value === "deposit" || formField.txn_type.value === "withdraw" ? this.modelFor("users.user.transactions").user.email : formField.beneficiary.value.split(" ")[1],
      "txn_type": formField.txn_type.value,
      "txn_amt": formField.txn_amt.value,
      "txn_status": "Success",
      "created_date": "",
      "last_modified_date": ""
    })
    try {
      await newT.save();
      this.router.refresh("users.user.transactions")
      this.router.transitionTo("users.user.transactions")
    } catch (error) {
      this.errorMessage = error.errors[0].detail
      console.error("Error: ", this.errorMessage)
      newT.rollbackAttributes();
    }
  }
}
