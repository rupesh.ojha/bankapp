import Route from '@ember/routing/route';
import {service} from "@ember/service";

export default class UsersUserBeneficiariesRoute extends Route {
  @service session
  @service router;
  @service store;

  beforeModel(_transition) {
    if(!this.session.isAuthenticated) {
      this.router.replaceWith('/login')
    }
  }
  async model() {
    // Get the user_id parameter from the route's model
    let user_stub = this.paramsFor('users.user').user_id;
    let [user, beneficiaries] = await Promise.all([
      this.store.findRecord('user', user_stub),
      this.store.findAll('beneficiaries', {user_id: user_stub}, {reload: true})
    ]);
    return {
      user,
      beneficiaries: Array.from(beneficiaries)
    };
  }
}
