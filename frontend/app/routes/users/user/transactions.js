import Route from '@ember/routing/route';
import {service} from "@ember/service";

export default class UsersUserTransactionsRoute extends Route {
  @service session
  @service router;
  @service store;

  beforeModel(_transition) {
    if(!this.session.isAuthenticated) {
      this.router.replaceWith('/login')
    }
  }
  async model() {
    // Get the user_id parameter from the route's model
    let user_stub = this.paramsFor('users.user').user_id;
    let [user, beneficiaries, transactions] = await Promise.all([
      this.store.findRecord('user', user_stub),
      this.store.findAll('beneficiaries'),
      this.store.findAll('transactions', {user_id: user_stub})
    ]);
    return {
      user,
      beneficiaries: Array.from(beneficiaries),
      transactions: Array.from(transactions)
    };
  }
}
