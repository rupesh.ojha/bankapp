import Route from '@ember/routing/route';
import {service} from "@ember/service";
import {action} from "@ember/object";
import {isEmpty} from "@ember/utils";

export default class UsersUserEditRoute extends Route {
  @service store
  @service router

  model() {
    return this.modelFor("users.user").user
  }
  @action
  async submitForm() {
    event.preventDefault()
    let formField = event.target.elements
    let user_stub = this.modelFor("users.user").user.user_stub;
    let newU = this.store.createRecord('user', user_stub)
    newU.setProperties({
      "user_stub": user_stub,
      "name": formField.name.value,
      "email": formField.email.value,
      "password": formField.password.value,
      "phone": formField.phone.value,
      "gender": formField.gender.value,
      "account_type": formField.account_type.value,
      "balance": formField.balance.value,
      "num_trans": "",
      "num_beneficiaries": "",
      "created_date": "",
      "last_modified_date": "",
      "is_admin": ""
    })
    try {
      await newU.save();
      this.router.refresh("users.user")
      this.router.transitionTo("users.user");
    } catch (error) {
      if (!isEmpty(error.errors)) {
        console.error("Error:", error.errors[0].detail);
      }
    }
  }
}
