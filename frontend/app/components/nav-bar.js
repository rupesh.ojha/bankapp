// app/components/login-form.js
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class NavBarComponent extends Component {

  @service session;

  @action
  invalidateSession() {
    this.session.invalidate();

  }
}
