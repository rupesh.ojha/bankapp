// app/components/login-form.js
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class LoginFormComponent extends Component {
  @service session;
  @service router;
  @tracked errorMessage = '';

  @action
  async submitLoginForm(e) {
    e.preventDefault();
    try {
      await this.session.authenticate('authenticator:token', { email: this.email, password: this.password })
      if (this.session.isAuthenticated) {
        e.target.reset()
        this.router.transitionTo(`/users/${this.session.data.authenticated.user_stub}/`);
      }
    } catch (error) {
      this.errorMessage = error.text;
      console.error('Login failed:', this.errorMessage);
    }
  }
}
