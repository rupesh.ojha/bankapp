// app/helpers/is-caption.js
import { helper } from '@ember/component/helper';

export default helper(function caseChange([b, c]) {
  if(c === "lowercase") return b.toLowerCase()
  else if(c === "uppercase") return b.toUpperCase
});
