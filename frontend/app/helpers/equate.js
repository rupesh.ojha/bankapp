// app/helpers/is-caption.js
import { helper } from '@ember/component/helper';

export default helper(function equate([a, b]) {
  return a == b;
});
