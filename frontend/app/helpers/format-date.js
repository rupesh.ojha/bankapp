import { helper } from '@ember/component/helper';

export default helper(function formatDate([epoch]) {
  let newDate = new Date(epoch)
  return newDate.toLocaleString()
});
