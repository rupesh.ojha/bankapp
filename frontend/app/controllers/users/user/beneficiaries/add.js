import Controller from '@ember/controller';
import {tracked} from "@glimmer/tracking";
import {service} from "@ember/service";
import {action} from "@ember/object";

export default class UsersUserBeneficiariesAddController extends Controller {
  @service store
  @service router
  @tracked errorMessage

  @action
  async submitForm() {
    event.preventDefault();
    let formField = event.target.elements
    this.errorMessage = ''
    // let user_stub = this.modelFor("users.user").user_stub;
    let newB = this.store.createRecord('beneficiaries', {
      "user_stub": this.model.user.user_stub,
      "beneficiary_stub": "",
      "email":formField.email.value,
      "bank":formField.bank.value,
      "ifsc":formField.ifsc.value,
      "acct_id":formField.acct_id.value
    });
    try {
      await newB.save();
      this.router.refresh("users.user.beneficiaries")
      this.router.transitionTo("users.user.beneficiaries")
    } catch (error) {
      this.errorMessage = error.errors[0].detail
      console.error("Error: ", this.errorMessage)
      newB.rollbackAttributes();
    }
  }
}
