import EmberRouter from '@ember/routing/router';
import config from 'bank-app/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('login');

  this.route('register');

  this.route('not-found', { path: '/*path' });

  // this.route('authenticated', {path: ''}, function() {
  //
  // })
  this.route('users', function() {
    this.route('user', {path: '/:user_id'}, function() {
      this.route('edit');
      this.route('beneficiaries', function() {
        this.route('add');
      });
      this.route('transactions', function() {
        this.route('add', function() {
          this.route('deposit');
          this.route('transfer');
          this.route('withdraw');
        });
      });
    });
  });

  this.route('admin', function() {
    this.route('users', function() {
      this.route('user', {path: '/:user_id'}, function() {
        this.route('edit');
      });
      this.route('add');
    });
  });

});
