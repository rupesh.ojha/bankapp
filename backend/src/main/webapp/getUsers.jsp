<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*, com.example.Model.UserModel"%>
<%  // Retrieve session attribute named "username"
  String loggedIn = (String) session.getAttribute("loggedIn");
  if(loggedIn != "true") {
    response.sendRedirect("welcome.jsp");
  }
  else {
      UserModel loggedIn_user = (UserModel) session.getAttribute("user");
      if(loggedIn_user.getIs_admin() == false) response.sendRedirect("index.jsp");
      else {
%>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Get Users | User Management</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <link rel="stylesheet" href="styles/index.css" />
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="index.jsp">BankApp</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="index.jsp">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="getBeneficiaries.jsp">Beneficiaries</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="getTransactions.jsp">Transactions</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Make Payment
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="addTransaction.jsp">Funds Transfer</a></li>
              <li><a class="dropdown-item" href="addDeposit.jsp">Deposit Funds</a></li>
              <li><a class="dropdown-item" href="addWithdraw.jsp">Withdraw Funds</a></li>
            </ul>
          </li>
        </ul>
        <form class="d-flex">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="updateUser.jsp?user_stub=<%= loggedIn_user.getUser_stub() %>&n=<%= loggedIn_user.getName()%>&e=<%= loggedIn_user.getEmail()%>&p=<%= loggedIn_user.getPhone()%>&g=<%= loggedIn_user.getGender()%>&at=<%= loggedIn_user.getAccount_type()%>&b=<%= loggedIn_user.getBalance()%>&nt=<%= loggedIn_user.getNum_trans() %>&nn=<%= loggedIn_user.getNum_beneficiaries() %>">
             Edit Profile</a>
          </li>
        </ul>
          <% if(loggedIn_user.getIs_admin() == true) { %>
            <a type="button" class="btn btn-primary mx-2" href="GetUsers">Admin</a>
          <% } %>
          <a type="button" class="btn btn-danger" href="Logout">Logout</a>
        </form>
      </div>
    </div>
  </nav>
  <div class="container">
    <h4 class="display-6 p-4">Admin Dashboard</h4>
    <div class="row p-3">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <!-- Table of Users -->
              <div class="table-caption">
                <div>
                    <h5 class="mb-0">Users</h5>
                    <p>List of all the active users</p>
                </div>
                <div>
                    <a type="button" href="addUser.jsp" class="btn btn-success mx-3">+ Add User</a>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-sm text-center">
                  <thead>
                    <tr>
                      <th scope="col">S.No.</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Account Type</th>
                      <th scope="col">Current Balance</th>
                      <th scope="col">Transactions</th>
                      <th scope="col">Edit / Delete User</th>
                    </tr>
                  </thead>
                  <tbody>
                <%
                        ArrayList<UserModel> users = (ArrayList<UserModel>) request.getAttribute("data");
                        if (users != null) {
                            int idx = 1;
                            for (UserModel user : users) { %>
                                <% if(user.getIs_admin() == false) { %>
                                    <tr>
                                      <td><%= idx++ %></td>
                                      <td><%= user.getName() %></td>
                                      <td><%= user.getEmail() %></td>
                                      <td><%= user.getPhone() %></td>
                                      <td><%= user.getGender() %></td>
                                      <td><%= user.getAccount_type() %></td>
                                      <td><%= user.getBalance() %></td>
                                      <td><%= user.getNum_trans() %></td>
                                      <td>
                                        <a type="button" class="btn btn-sm btn-primary mx-1"
                                          href="updateUser.jsp?user_stub=<%= user.getUser_stub() %>&n=<%= user.getName()%>&e=<%= user.getEmail()%>&p=<%= user.getPhone()%>&g=<%= user.getGender()%>&at=<%= user.getAccount_type()%>&b=<%= user.getBalance()%>&nt=<%= user.getNum_trans() %>&nn=<%= user.getNum_beneficiaries() %>">✏️</a>
                                        <a type="button" class="btn btn-sm btn-danger mx-1" href="DeleteUser?user_stub=<%= user.getUser_stub() %>">X</a>
                                      </td>
                                    </tr>
                                <% } %>
                            <% }
                        }
                    %>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
    crossorigin="anonymous"></script>
</body>
</html>
<% } } %>