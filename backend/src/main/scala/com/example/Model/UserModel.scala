package com.example.Model

import java.sql.Timestamp

case class UserModel(
  user_stub: String = null,
  name: String,
  email: String,
  password: String,
  phone: String,
  gender: String,
  account_type: String,
  balance: Integer,
  num_trans: Integer,
  num_beneficiaries: Integer,
  created_date: Timestamp,
  last_modified_date: Timestamp,
  is_admin: Boolean
)