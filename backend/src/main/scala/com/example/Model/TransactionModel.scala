package com.example.Model

import java.sql.Timestamp

case class TransactionModel (
  txn_stub: String = null,
  payer_stub: String,
  payee_stub: String,
  payer_email: String,
  payee_email: String,
  txn_type: String,
  txn_amt: Integer,
  txn_status: String,
  created_date: Timestamp,
  last_modified_date: Timestamp
)