package com.example.Model

case class BeneficiaryModel (
  user_stub: String,
  beneficiary_stub: String = null,
  email: String,
  bank: String,
  ifsc: String,
  acct_id: Integer
)