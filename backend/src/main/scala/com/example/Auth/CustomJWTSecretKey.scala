package com.example.Auth

import javax.crypto.SecretKey
import javax.crypto.KeyGenerator
import javax.crypto.spec.SecretKeySpec
import java.util.Base64

object CustomJWTSecretKey {

  // Default secret key
  private var secretKey: SecretKey = generateRandomKey()

  // Method to get the secret key
  def getSecretKey: SecretKey = this.secretKey

  // Method to generate a random secret key
  private def generateRandomKey(): SecretKey = {
    val keyGenerator = KeyGenerator.getInstance("HmacSHA256")
    keyGenerator.init(256) // You can set the key size as needed
    keyGenerator.generateKey()
  }
}
