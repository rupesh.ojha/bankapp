package com.example.Auth

import io.jsonwebtoken.Jwts

import javax.servlet._
import javax.servlet.http._

class CustomFilter extends Filter {
  override def init(filterConfig: FilterConfig): Unit = {}

  override def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain): Unit = {
    val httpRequest = request.asInstanceOf[HttpServletRequest]
    val httpResponse = response.asInstanceOf[HttpServletResponse]

    // Add CORS headers to allow requests from all origins
    httpResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
    httpResponse.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
    httpResponse.setHeader("Access-Control-Max-Age", "3600")
    httpResponse.setHeader("Access-Control-Allow-Origin", "*")

    // Check if the request URL matches /users/*
    if (httpRequest.getRequestURI.contains("/users/")) {
      if (httpRequest.getMethod == "OPTIONS") {
        // Handle OPTIONS request, set appropriate headers and return
        httpResponse.setStatus(200)
        httpResponse.setHeader("Access-Control-Allow-Origin", "*")
        httpResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
        httpResponse.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
        httpResponse.setHeader("Access-Control-Max-Age", "3600")
        return
      }

      // Apply AuthenticationFilter logic
      val token: Option[String] = Option(httpRequest.getHeader("Authorization")).map(_.replace("Bearer ", ""))
      // Verify the token
      token match {
        case Some(jwt) =>
          try {
            val key = CustomJWTSecretKey.getSecretKey
            Jwts.parser().verifyWith(key).build().parseSignedClaims(jwt)
            if(httpRequest.getMethod == "OPTIONS") httpResponse.setStatus(200)
            chain.doFilter(request, response)
          } catch {
            case _: Exception =>
              // Token is invalid, return unauthorized response
              httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED)
              httpResponse.getWriter.write("Unauthorized. Token is Invalid.")
          }
        case None =>
          // Token not provided, return unauthorized response
          httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED)
          httpResponse.getWriter.write("Unauthorized. Token not found.")
      }
    } else {
      // If not /users/*, allow all origins
      chain.doFilter(request, response)
    }
  }

  override def destroy(): Unit = {}
}
