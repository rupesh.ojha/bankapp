package com.example.Util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.example.Model.{BeneficiaryModel, TransactionModel, UserModel}
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import java.util

object ParseJSON {
  def convertModelToJson(user: UserModel = null, bens: BeneficiaryModel = null, txns: TransactionModel = null): String = {
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    val mainObj: ObjectNode = objectMapper.createObjectNode()

    val userObj = objectMapper.createObjectNode()
    if(user != null) {
      userObj.put("id", user.user_stub)
      userObj.put("type", "user")
      userObj.put("attributes", objectMapper.readTree(objectMapper.writeValueAsString(user)))
    }
    else if(bens != null) {
      userObj.put("id", bens.email)
      userObj.put("type", "beneficiaries")
      userObj.put("attributes", objectMapper.readTree(objectMapper.writeValueAsString(bens)))
    }
    else {
      userObj.put("id", txns.txn_stub)
      userObj.put("type", "transactions")
      userObj.put("attributes", objectMapper.readTree(objectMapper.writeValueAsString(txns)))
    }
    mainObj.put("data", userObj)

    objectMapper.writeValueAsString(mainObj)
  }
  def convertArrToJson(beneficiaries: util.ArrayList[BeneficiaryModel] = null, txns: util.ArrayList[TransactionModel] = null, users: util.ArrayList[UserModel] = null): String = {
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    val mainObj: ObjectNode = objectMapper.createObjectNode()

    val dataArr = objectMapper.createArrayNode()
    if(beneficiaries != null) {
      beneficiaries.forEach(b => {
        val bObj = objectMapper.createObjectNode()
        bObj.put("id", b.email)
        bObj.put("type", "beneficiaries")
        bObj.put("attributes", objectMapper.readTree(objectMapper.writeValueAsString(b)))

        dataArr.add(bObj)
      })
    }
    else if(txns != null) {
      txns.forEach(t => {
        val tObj = objectMapper.createObjectNode()
        tObj.put("id", t.txn_stub)
        tObj.put("type", "transactions")
        tObj.put("attributes", objectMapper.readTree(objectMapper.writeValueAsString(t)))

        dataArr.add(tObj)
      })
    }
    else {
      users.forEach(u => {
        val tObj = objectMapper.createObjectNode()
        tObj.put("id", u.user_stub)
        tObj.put("type", "user")
        tObj.put("attributes", objectMapper.readTree(objectMapper.writeValueAsString(u)))

        dataArr.add(tObj)
      })
    }

    mainObj.put("data", dataArr)

    objectMapper.writeValueAsString(mainObj)
  }
}
