package com.example.DAO

import com.example.Model.UserModel

import java.sql.{Connection, DriverManager, Timestamp}
import java.time
import java.util.{ArrayList, UUID}

object UserDAO {
  // @desc  Creates and returns a connection to the DB
  // @controller  N/A
  private def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "postgres")
  }
  // @desc  Adds a user to the DB
  // @controller  /AddUser
  def addUser(user: UserModel): Integer = {
    var insertedRows = 0
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an insertion statement, populating the params, and executing it
      val insertStatement = connection.prepareStatement("INSERT INTO accounts (name, email, password, phone, gender, account_type, balance, num_trans, num_beneficiaries, created_date, last_modified_date, is_admin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
      insertStatement.setString(1, user.name)
      insertStatement.setString(2, user.email)
      insertStatement.setString(3, user.password)
      insertStatement.setString(4, user.phone)
      insertStatement.setString(5, user.gender)
      insertStatement.setString(6, user.account_type)
      insertStatement.setInt(7, user.balance)
      insertStatement.setInt(8, user.num_trans)
      insertStatement.setInt(9, user.num_beneficiaries)
      val curDT : Timestamp = Timestamp.valueOf(time.LocalDateTime.now())
      insertStatement.setTimestamp(10, curDT)
      insertStatement.setTimestamp(11, curDT)
      insertStatement.setBoolean(12, user.is_admin)
      insertedRows = insertStatement.executeUpdate

      // Closing DB Connections
      insertStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    insertedRows
  }

  // @desc  Checks if a user already exists
  // @controller  N/A
  def checkUser(email: String): Boolean = {
    var exists: Boolean = false
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an select statement and executing it
      val selectStatement = connection.prepareStatement("SELECT count(1) from accounts where email = ?;")
      selectStatement.setObject(1, email)

      val fetchedUser = selectStatement.executeQuery()
      while(fetchedUser.next()) {
        val count = fetchedUser.getInt("count")
        if (count > 0) {
          exists = true
        }
      }

      // Closing resources
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    exists
  }

  // @desc  Returns all the users
  // @controller  /GetUsers
  def getUsers: ArrayList[UserModel] = {
    val users = new ArrayList[UserModel]
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an select statement and executing it
      val selectStatement = connection.createStatement
      val fetchedUsers = selectStatement.executeQuery("select user_stub, name, email, phone, gender, account_type, balance, num_trans, num_beneficiaries, created_date, last_modified_date, is_admin from accounts order by created_date;")

      while (fetchedUsers.next) {
        val user = UserModel(
          user_stub = fetchedUsers.getString("user_stub"),
          name = fetchedUsers.getString("name"),
          email = fetchedUsers.getString("email"),
          password = null,
          phone = fetchedUsers.getString("phone"),
          gender = fetchedUsers.getString("gender"),
          account_type = fetchedUsers.getString("account_type"),
          balance = fetchedUsers.getInt("balance"),
          num_trans = fetchedUsers.getInt("num_trans"),
          num_beneficiaries = fetchedUsers.getInt("num_beneficiaries"),
          created_date = fetchedUsers.getTimestamp("created_date"),
          last_modified_date = fetchedUsers.getTimestamp("last_modified_date"),
          is_admin = fetchedUsers.getBoolean("is_admin")
        )
        users.add(user)
      }

      // Closing resources
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    users
  }

  // @desc  Returns a single user details based on stub
  // @controller  N/A
  def getUserByStub(user_stub: String): UserModel = {
    var user: UserModel = null
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an select statement and executing it
      val selectStatement = connection.prepareStatement("select user_stub, name, email, phone, gender, account_type, balance, num_trans, num_beneficiaries, created_date, last_modified_date, is_admin from accounts where user_stub = ?;")
      selectStatement.setObject(1, UUID.fromString(user_stub))
      val fetchedUser = selectStatement.executeQuery()

      while (fetchedUser.next) {
        user = UserModel(
          user_stub = fetchedUser.getString("user_stub"),
          name = fetchedUser.getString("name"),
          email = fetchedUser.getString("email"),
          password = null,
          phone = fetchedUser.getString("phone"),
          gender = fetchedUser.getString("gender"),
          account_type = fetchedUser.getString("account_type"),
          balance = fetchedUser.getInt("balance"),
          num_trans = fetchedUser.getInt("num_trans"),
          num_beneficiaries = fetchedUser.getInt("num_beneficiaries"),
          created_date = fetchedUser.getTimestamp("created_date"),
          last_modified_date = fetchedUser.getTimestamp("last_modified_date"),
          is_admin = fetchedUser.getBoolean("is_admin")
        )
      }
      // Closing resources
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    user
  }

  // @desc  Deletes a user based on the ID
  // @controller  /DeleteUser
  def deleteUser(user_stub: String): Integer = {
    var deletedRows = 0
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing a deletion statement, populating the params, and executing it
      val deleteStatement = connection.prepareStatement("delete from accounts where user_stub = ?")
      deleteStatement.setObject(1, UUID.fromString(user_stub))
      deletedRows = deleteStatement.executeUpdate

      // Closing DB Connection
      deleteStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    deletedRows
  }

  // @desc  Updates the user details with a newer version
  // @controller  /UpdateUser
  def editUser(user: UserModel): Integer = {
    var updatedRows = 0
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an updation statement, populating the params, and executing it
      val updateStatement = connection.prepareStatement("update accounts set name = ?, email = ?, password = ?, phone = ?, gender = ?, account_type = ?, balance = ?, last_modified_date = ? where user_stub = ?")
      updateStatement.setString(1, user.name)
      updateStatement.setString(2, user.email)
      updateStatement.setString(3, user.password)
      updateStatement.setString(4, user.phone)
      updateStatement.setString(5, user.gender)
      updateStatement.setString(6, user.account_type)
      updateStatement.setInt(7, user.balance)
      // not updating # of transactions & beneficiaries
      val curDT : Timestamp = Timestamp.valueOf(time.LocalDateTime.now())
      updateStatement.setTimestamp(8, curDT)
      updateStatement.setObject(9, UUID.fromString(user.user_stub))
      updatedRows = updateStatement.executeUpdate

      // Closing DB Connections
      updateStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    updatedRows
  }

  // @desc  Handles user authentication and login
  // @controller  /Login
  def login(email: String, pw: String): UserModel = {
    var user: UserModel = null
    var exists = false
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing a select statement, populating the params, and executing it
      val selectStatement = connection.prepareStatement("select user_stub, name, email, phone, gender, account_type, balance, num_trans, num_beneficiaries, created_date, last_modified_date, is_admin from accounts where email = ? and password = ?;")
      selectStatement.setString(1, email)
      selectStatement.setString(2, pw)
      val fetchedUser = selectStatement.executeQuery

      while (fetchedUser.next) {
        exists = true
        user = UserModel(
          user_stub = fetchedUser.getString("user_stub"),
          name = fetchedUser.getString("name"),
          email = fetchedUser.getString("email"),
          password = null,
          phone = fetchedUser.getString("phone"),
          gender = fetchedUser.getString("gender"),
          account_type = fetchedUser.getString("account_type"),
          balance = fetchedUser.getInt("balance"),
          num_trans = fetchedUser.getInt("num_trans"),
          num_beneficiaries = fetchedUser.getInt("num_beneficiaries"),
          created_date = fetchedUser.getTimestamp("created_date"),
          last_modified_date = fetchedUser.getTimestamp("last_modified_date"),
          is_admin = fetchedUser.getBoolean("is_admin")
        )
      }

      // Closing resources
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    if (exists) user
    else null
  }
}
