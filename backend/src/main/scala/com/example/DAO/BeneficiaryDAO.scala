package com.example.DAO

import com.example.Model.BeneficiaryModel

import java.sql.{Connection, DriverManager}
import java.util.UUID
import java.util

object BeneficiaryDAO {
  // @desc  Creates and returns a connection to the DB
  // @controller  N/A
  private def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "postgres")
  }

  // @desc  Adds a beneficiary to the DB
  // @controller  /AddBeneficiary
  def addBeneficiary(beneficiary: BeneficiaryModel) : Integer = {
    var insertedRecords: Integer = 0
    try {
      // Getting DB Connection
      val connection = getConnection

      // Try to fetch an 'existing user' (of the same bank) via the email provided
      val selectStatement = connection.prepareStatement("SELECT user_stub FROM accounts where email = ?;")
      selectStatement.setString(1, beneficiary.email)
      val rs = selectStatement.executeQuery()
      var existingUser: String = null
      while(rs.next()) {
        existingUser = rs.getString("user_stub")
      }

      val insertStatement = connection.prepareStatement("INSERT INTO beneficiaries (user_stub, beneficiary_stub, email, bank, ifsc, acct_id) VALUES(?, ?, ?, ?, ?, ?);")
      insertStatement.setObject(1, UUID.fromString(beneficiary.user_stub))

      if(existingUser == null) { // If the beneficiary doesn't exist as a user: add the bank a/c details
        insertStatement.setObject(2, null)
        insertStatement.setString(3, beneficiary.email)
        insertStatement.setString(4, beneficiary.bank)
        insertStatement.setString(5, beneficiary.ifsc)
        insertStatement.setInt(6, beneficiary.acct_id)
      } else { // If the beneficiary exists as a user: add his/her user_stub ONLY
        insertStatement.setObject(2, UUID.fromString(existingUser))
        insertStatement.setString(3, beneficiary.email)
        insertStatement.setString(4, null)
        insertStatement.setString(5, null)
        insertStatement.setObject(6, null)
      }

      // Update the 'accounts' table for user with INCREMENTED num_beneficiaries
      val updateStatement = connection.prepareStatement("UPDATE accounts SET num_beneficiaries = num_beneficiaries + 1 WHERE user_stub = ?;")
      updateStatement.setObject(1, UUID.fromString(beneficiary.user_stub))

      insertedRecords = insertStatement.executeUpdate()
      updateStatement.executeUpdate()

      // Closing DB Connections
      selectStatement.close()
      insertStatement.close()
      updateStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }

    insertedRecords
  }

  // @desc Get ALL beneficiaries associated with a user
  // @controller  /GetBeneficiaries
  def getBeneficiaries(user_stub: String): util.ArrayList[BeneficiaryModel] = {
    val beneficiaries: util.ArrayList[BeneficiaryModel] = new util.ArrayList[BeneficiaryModel]()
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an select statement and executing it
      val selectStatement = connection.prepareStatement("SELECT user_stub, beneficiary_stub, email, bank, ifsc, acct_id from beneficiaries where user_stub = ?;")
      selectStatement.setObject(1, UUID.fromString(user_stub))

      val fetchedBeneficiaries = selectStatement.executeQuery()
      while(fetchedBeneficiaries.next()) {
        val beneficiary: BeneficiaryModel = BeneficiaryModel(
          user_stub = fetchedBeneficiaries.getString("user_stub"),
          beneficiary_stub = fetchedBeneficiaries.getString("beneficiary_stub"),
          email = fetchedBeneficiaries.getString("email"),
          bank = if(fetchedBeneficiaries.getString("bank") == null) "BankApp" else fetchedBeneficiaries.getString("bank"),
          ifsc = if(fetchedBeneficiaries.getString("ifsc") == null) "BA0001" else fetchedBeneficiaries.getString("ifsc"),
          acct_id = if(fetchedBeneficiaries.getObject("acct_id") == null) 100001 else fetchedBeneficiaries.getInt("acct_id"),
        )
        beneficiaries.add(beneficiary)
      }

      // Closing resources
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    beneficiaries
  }

  // @desc Delete a beneficiary associated with a user
  // @controller  /DeleteBeneficiary
  def deleteBeneficiary(user_stub: String, email: String, all: Boolean): Integer = {
    var deletedRows: Integer = 0
    try {
      // Getting DB Connection
      val connection = getConnection

      if(all) { // Case when the user account gets deleted, ALL associated beneficiaries have to be deleted as well
          val deleteStatement = connection.prepareStatement("DELETE FROM beneficiaries WHERE user_stub = ?;")
          deleteStatement.setObject(1, UUID.fromString(user_stub))
          deletedRows = deleteStatement.executeUpdate()
      } else { // Single beneficiary deletion
          val deleteStatement = connection.prepareStatement("DELETE FROM beneficiaries WHERE user_stub = ? and email = ?;")
          deleteStatement.setObject(1, UUID.fromString(user_stub))
          deleteStatement.setString(2, email)

          // Update the 'accounts' table for user with DECREMENTED num_beneficiaries
          val updateStatement1 = connection.prepareStatement("UPDATE accounts SET num_beneficiaries = num_beneficiaries - 1 WHERE user_stub = ?;")
          updateStatement1.setObject(1, UUID.fromString(user_stub))

          deletedRows = deleteStatement.executeUpdate()
          updateStatement1.executeUpdate()
          connection.close()
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    deletedRows
  }

  // @desc Get ALL beneficiaries associated with a user
  // @controller  /GetBeneficiaries
  def checkBeneficiary(user_stub: String, beneficiary_email: String): Boolean = {
    var exists: Boolean = false
    try {
      // Getting DB Connection
      val connection = getConnection

      // Preparing an select statement and executing it
      val selectStatement = connection.prepareStatement("SELECT count(1) from beneficiaries where user_stub = ? and email = ?;")
      selectStatement.setObject(1, UUID.fromString(user_stub))
      selectStatement.setObject(2, beneficiary_email)

      val fetchedBeneficiaries = selectStatement.executeQuery()
      while(fetchedBeneficiaries.next()) {
        val count = fetchedBeneficiaries.getInt("count")
        if (count > 0) {
          exists = true
        }
      }

      // Closing resources
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    exists
  }
}