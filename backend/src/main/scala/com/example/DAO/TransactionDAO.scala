package com.example.DAO

import com.example.Model.TransactionModel

import java.sql.{Connection, DriverManager, Timestamp}
import java.{time, util}
import java.util.UUID

object TransactionDAO {
  // @desc  Creates and returns a connection to the DB
  // @controller  N/A
  private def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "postgres")
  }
  // @desc  Gets all transactions associated with a user
  // @controller  /GetTransactions
  def getTransactions(user_stub: String) : util.ArrayList[TransactionModel] = {
    val txns: util.ArrayList[TransactionModel] = new util.ArrayList[TransactionModel]
    try {
      // Getting DB Connection
      val connection = getConnection

      val selectStatement = connection.prepareStatement("SELECT * FROM transactions WHERE payer_stub = ? OR payee_stub = ?;")
      selectStatement.setObject(1, UUID.fromString(user_stub))
      selectStatement.setObject(2, UUID.fromString(user_stub))

      val rs = selectStatement.executeQuery()
      while(rs.next()) {
        val tm = TransactionModel(
          txn_stub = rs.getString("txn_stub"),
          payer_stub = rs.getString("payer_stub"),
          payee_stub = rs.getString("payee_stub"),
          payer_email = rs.getString("payer_email"),
          payee_email = rs.getString("payee_email"),
          txn_type = rs.getString("txn_type"),
          txn_amt = rs.getInt("txn_amt"),
          txn_status = rs.getString("txn_status"),
          created_date = rs.getTimestamp("created_date"),
          last_modified_date = rs.getTimestamp("last_modified_date")
        )
        txns.add(tm)
      }

      // Closing DB Connections
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception => e.printStackTrace()
    }
    txns
  }

  // @desc  Gets a transaction associated with a user by stub
  // @controller  /GetTransactionByStub
  def getTransactionByStub(txn_stub: String) : TransactionModel = {
    var txn: TransactionModel = null
    try {
      // Getting DB Connection
      val connection = getConnection

      val selectStatement = connection.prepareStatement("SELECT * FROM transactions WHERE txn_stub = ?;")
      selectStatement.setObject(1, UUID.fromString(txn_stub))

      val rs = selectStatement.executeQuery()
      while(rs.next()) {
        txn = TransactionModel(
          txn_stub = rs.getString("txn_stub"),
          payer_stub = rs.getString("payer_stub"),
          payee_stub = rs.getString("payee_stub"),
          payer_email = rs.getString("payer_email"),
          payee_email = rs.getString("payee_email"),
          txn_type = rs.getString("txn_type"),
          txn_amt = rs.getInt("txn_amt"),
          txn_status = rs.getString("txn_status"),
          created_date = rs.getTimestamp("created_date"),
          last_modified_date = rs.getTimestamp("last_modified_date")
        )
      }

      // Closing DB Connections
      selectStatement.close()
      connection.close()
    } catch {
      case e: Exception => e.printStackTrace()
    }
    txn
  }

  // @desc  Adds a transaction to the DB
  // @controller  /AddTransaction
  def addTransaction(txn: TransactionModel): TransactionModel = {
    var newTxn: TransactionModel = null
    try {
      // Getting DB Connection
      val connection = getConnection

      // Add a Transaction record to the 'transaction' DB
      val insertStatement = connection.prepareStatement("INSERT INTO transactions (payer_stub, payee_stub, payer_email, payee_email, txn_type, txn_amt, txn_status, created_date, last_modified_date) values(?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING txn_stub")
      insertStatement.setObject(1, UUID.fromString(txn.payer_stub))
      insertStatement.setObject(2, UUID.fromString(txn.payee_stub))
      insertStatement.setString(3, txn.payer_email)
      insertStatement.setString(4, txn.payee_email)
      insertStatement.setObject(5, txn.txn_type)
      insertStatement.setInt(6, txn.txn_amt)
      insertStatement.setString(7, txn.txn_status)
      val curDT : Timestamp = Timestamp.valueOf(time.LocalDateTime.now())
      insertStatement.setTimestamp(8, curDT)
      insertStatement.setTimestamp(9, curDT)
      val rs = insertStatement.executeQuery()
      if (rs.next()) {
        newTxn = getTransactionByStub(rs.getObject("txn_stub").toString)
      }

      if (txn.txn_type == "transfer") {
        // Update the 'accounts' table for both 'payer' and 'payee' with UPDATED balances, and INCREMENTED num_trans
        val updateStatement1 = connection.prepareStatement("UPDATE accounts SET balance = balance - ?, num_trans = num_trans + 1 WHERE user_stub = ?;")
        val updateStatement2 = connection.prepareStatement("UPDATE accounts SET balance = balance + ?, num_trans = num_trans + 1 WHERE user_stub = ?;")
        updateStatement1.setInt(1, txn.txn_amt)
        updateStatement1.setObject(2, UUID.fromString(txn.payer_stub))
        updateStatement2.setInt(1, txn.txn_amt)
        updateStatement2.setObject(2, UUID.fromString(txn.payee_stub))
        updateStatement1.executeUpdate()
        updateStatement2.executeUpdate()

        // Closing DB Connections
        insertStatement.close()
        updateStatement1.close()
        updateStatement2.close()
      }
      else if (txn.txn_type == "deposit") {
        // Update the 'accounts' table for user with UPDATED balance
        val updateStatement = connection.prepareStatement("UPDATE accounts SET balance = balance + ?, num_trans = num_trans + 1 WHERE user_stub = ?;")
        updateStatement.setInt(1, txn.txn_amt)
        updateStatement.setObject(2, UUID.fromString(txn.payer_stub))
        updateStatement.executeUpdate()

        // Closing DB Connections
        updateStatement.close()
      }
      else if (txn.txn_type == "withdraw"){
        // Update the 'accounts' table for user with UPDATED balance
        val updateStatement0 = connection.prepareStatement("UPDATE accounts SET balance = balance - ?, num_trans = num_trans + 1 WHERE user_stub = ?;")
        updateStatement0.setInt(1, txn.txn_amt)
        updateStatement0.setObject(2, UUID.fromString(txn.payer_stub))
        updateStatement0.executeUpdate()

        // Closing DB Connections
        updateStatement0.close()
      }
      connection.close()
    } catch {
      case e: Exception => e.printStackTrace()
    }
    newTxn
  }
}
