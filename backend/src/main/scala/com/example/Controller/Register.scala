package com.example.Controller

import com.example.Auth.CustomJWTSecretKey
import com.example.DAO.{BeneficiaryDAO, TransactionDAO, UserDAO}
import com.example.Model.{BeneficiaryModel, TransactionModel, UserModel}
import com.example.Util.ParseJSON

import java.{lang, util}
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys

import java.security.Key

@WebServlet(Array("/Register"))
@SerialVersionUID(1L)
class Register extends HttpServlet {
  @throws[ServletException]
  override def doPost(req: HttpServletRequest, res: HttpServletResponse): Unit = {
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    res.setContentType("application/json")
    res.setCharacterEncoding("UTF-8")

    val newRecord= objectMapper.readValue(req.getReader, classOf[UserModel])
    if (UserDAO.checkUser(newRecord.email)) { // Check if user already exists
      res.setStatus(500)
      res.getWriter.write("User already exists!")
    }
    else {
      val insertedRecords: Integer = UserDAO.addUser(newRecord)  // Insert User
      if (insertedRecords > 0) {
        res.setStatus(200)
        res.getWriter.write("User created successfully.")
//        res.getWriter.write(ParseJSON.convertModelToJson(null, newRecord, null))
      }
      else {
        res.setStatus(500)
        res.getWriter.write("Failed to add beneficiary! Try again.}")
      }
    }
  }
}