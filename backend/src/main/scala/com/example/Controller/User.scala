package com.example.Controller

import com.example.DAO.{BeneficiaryDAO, TransactionDAO, UserDAO}
import com.example.Model.{BeneficiaryModel, TransactionModel, UserModel}
import com.example.Util.ParseJSON
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import java.util
import java.util.ArrayList
import javax.servlet.annotation.WebServlet
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import javax.servlet.{RequestDispatcher, ServletException}

@WebServlet(Array("/users/*"))
@SerialVersionUID(1L)
class User extends HttpServlet {
  @throws[ServletException]
  override def doGet(req: HttpServletRequest, res: HttpServletResponse): Unit = {
    val user_stub = req.getPathInfo.split("/")(1)
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    res.setContentType("application/json")
    res.setCharacterEncoding("UTF-8")

    val segments = req.getPathInfo.substring(1).split("/")
    // requests to /user/:user_stub/beneficiaries
    if (segments.length == 2 && segments(1) == "beneficiaries") {
      val beneficiaries: util.ArrayList[BeneficiaryModel] = BeneficiaryDAO.getBeneficiaries(user_stub)
      res.getWriter.write(ParseJSON.convertArrToJson(beneficiaries, null))
    }
    // requests to /user/:user_stub/transactions
    else if (segments.length == 2 && segments(1) == "transactions") {
      val txns: util.ArrayList[TransactionModel] = TransactionDAO.getTransactions(user_stub)
      res.getWriter.write(ParseJSON.convertArrToJson(null, txns))
    }
    // requests to /user/:user_stub
    else {
      val user: UserModel = UserDAO.getUserByStub(user_stub)
      res.getWriter.write(ParseJSON.convertModelToJson(user, null, null))
    }
  }
  @throws[ServletException]
  override def doPost(req: HttpServletRequest, res: HttpServletResponse): Unit = {
    val user_stub = req.getPathInfo.split("/")(1)
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    res.setContentType("application/json")
    res.setCharacterEncoding("UTF-8")

    val segments = req.getPathInfo.substring(1).split("/")

    // requests to /user/:user_stub
    if(segments.length == 1) {
      val editedRows = UserDAO.editUser(objectMapper.readValue(req.getReader, classOf[UserModel]))
      if (editedRows > 0) {
        val user: UserModel = UserDAO.getUserByStub(user_stub)
        res.setStatus(201)
        res.getWriter.write(ParseJSON.convertModelToJson(user, null, null))
      }
      else {
        res.setStatus(500)
        res.getWriter.write("Failed to edit user! Try again.")
      }
    }


    // requests to /user/:user_stub/beneficiaries/add
    else if(segments.length == 3 && segments(1) == "beneficiaries") {
      val newRecord = objectMapper.readValue(req.getReader, classOf[BeneficiaryModel])
       if (newRecord.email == UserDAO.getUserByStub(user_stub).email) {  // Self-addition prevention
         res.setStatus(500)
         res.getWriter.write("Cannot add Self as a beneficiary!")
       }
       else if (BeneficiaryDAO.checkBeneficiary(user_stub, newRecord.email)) { // Check if beneficiary already exists
         res.setStatus(500)
         res.getWriter.write("Beneficiary already exists!")
       }
       else {
         val insertedRecords: Integer = BeneficiaryDAO.addBeneficiary(newRecord)  // Insert beneficiary
         if (insertedRecords > 0) {
           res.setStatus(200)
           res.getWriter.write(ParseJSON.convertModelToJson(null, newRecord, null))
         }
         else {
           res.setStatus(500)
           res.getWriter.write("Failed to add beneficiary! Try again.}")
         }
       }
    }


    // requests to /user/:user_stub/transactions/add
    else if(segments.length == 3 && segments(1) == "transactions" && segments(2) == "add") {
      val newRecord = objectMapper.readValue(req.getReader, classOf[TransactionModel])
      if(newRecord.txn_type == "transfer" && newRecord.txn_amt > UserDAO.getUserByStub(user_stub).balance
        || newRecord.txn_type == "withdraw" && newRecord.txn_amt > UserDAO.getUserByStub(user_stub).balance) {
        res.setStatus(500)
        res.getWriter.write("Transaction amount cannot be greater than the current balance!")
      }
      else {
        val insertedRecord: TransactionModel = TransactionDAO.addTransaction(newRecord)
        if (insertedRecord != null) {
          res.setStatus(200)
          res.getWriter.write(ParseJSON.convertModelToJson(null, null, insertedRecord))
        }
        else {
          res.setStatus(500)
          res.getWriter.write("Failed to add transaction! Try again.")
        }
      }
    }
  }
  @throws[ServletException]
  override def doPut(req: HttpServletRequest, res: HttpServletResponse): Unit = {
    val user_stub = req.getPathInfo.split("/")(1)
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    res.setContentType("application/json")
    res.setCharacterEncoding("UTF-8")

    val segments = req.getPathInfo.substring(1).split("/")
    // requests to /user/:user_stub/edit
    if (segments.length == 2 && segments(1) == "edit") {
      val editedRows = UserDAO.editUser(objectMapper.readValue(req.getReader, classOf[UserModel]))
      if (editedRows > 0) {
        res.setStatus(200)
      }
      else {
        res.setStatus(500)
      }
    }
  }
  @throws[ServletException]
  override def doDelete(req: HttpServletRequest, res: HttpServletResponse): Unit = {
    val user_stub = req.getPathInfo.split("/")(1)
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    res.setContentType("application/json")
    res.setCharacterEncoding("UTF-8")

    val segments = req.getPathInfo.substring(1).split("/")
    // requests to /user/:user_stub
    if (segments.length == 1) {
      val deletedRows = UserDAO.deleteUser(user_stub)
      if (deletedRows > 0) {
        res.setStatus(200)
        res.getWriter.write("User deleted successfully.")
      }
      else {
        res.setStatus(500)
        res.getWriter.write("Failed to deleted the user.")
      }
    }
    // requests to /user/:user_stub/beneficiaries/:beneficiary_email
    else if (segments.length == 3 && segments(1) == "beneficiaries") {
      // Process the deleted operation
      val deletedRecords: Integer = BeneficiaryDAO.deleteBeneficiary(segments(0), segments(2), all = false)
      if (deletedRecords > 0) {
        res.setStatus(200)
      }
      else {
        res.setStatus(500)

      }
    }
  }
}
