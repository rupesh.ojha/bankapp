package com.example.Controller

import com.example.Auth.CustomJWTSecretKey
import com.example.DAO.{BeneficiaryDAO, TransactionDAO, UserDAO}
import com.example.Model.{BeneficiaryModel, TransactionModel, UserModel}

import java.{lang, util}
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys

import java.security.Key

@WebServlet(Array("/Login"))
@SerialVersionUID(1L)
class Login extends HttpServlet {
  @throws[ServletException]
  override protected def doPost(req: HttpServletRequest, res: HttpServletResponse): Unit = {
    // Read JSON data from request body
    val body: StringBuilder = new StringBuilder
    val reader = req.getReader
    var line: String = null
    while ({line = reader.readLine; line != null}) {
      body.append(line)
    }

    // Parse JSON data
    val objectMapper = new ObjectMapper()
    val jsonNode = objectMapper.readTree(body.toString)

    // Retrieve email and password from JSON data
    val email: String = jsonNode.get("email").asText()
    val password: String = jsonNode.get("password").asText()

    // Authenticate the user via the given details, return user OR null based on the results
    val user = UserDAO.login(email, password)
    if (user == null) {
      // Handle authentication failure
      res.setContentType("application/json")
      res.setStatus(401)
      res.getWriter.write("Invalid credentials! Try again.")
    } else {
      // JWT Auth
      val key = CustomJWTSecretKey.getSecretKey
      val jws = Jwts.builder.subject(user.user_stub).signWith(key).compact

      // Create a JSON object to hold the token
      val objectMapper = new ObjectMapper()
      objectMapper.registerModule(DefaultScalaModule)
      val responseObject: ObjectNode = objectMapper.createObjectNode()
      responseObject.put("token", jws)
      responseObject.put("user_stub", user.user_stub)

      // Convert the JSON object to a string
      val responseJson = objectMapper.writeValueAsString(responseObject)

      // Set the response content type and write the JSON string to the response body
      res.setContentType("application/json")
      res.setCharacterEncoding("UTF-8")
      res.getWriter.write(responseJson)
    }
  }
}